import { Component } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  test = "";
  constructor(userSer:UserService){
    this.test = userSer.getUser();
  }
  firstName:string='Sara';
  title = 'app';
}
