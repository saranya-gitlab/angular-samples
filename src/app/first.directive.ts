import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appFirst]'
})
export class FirstDirective {
  constructor(elRef: ElementRef) {  
    elRef.nativeElement.style.color = 'blue';  
 }  
}
