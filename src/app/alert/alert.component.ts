import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  // alert= 'default';
  @Input() alert;
  @Output() sayHello = new EventEmitter<String>();
  @Output() setChildValue = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

 hello(){
   console.log('I am hello');
   this.sayHello.emit('I am hello from child');
 }
}
