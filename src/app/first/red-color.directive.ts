import { Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appRedColor]'
})
export class RedColorDirective {

  constructor(elRef: ElementRef) {  
    elRef.nativeElement.style.color = 'red';  
 }  

}
