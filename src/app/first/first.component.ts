import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  
  model: any = {};

  onSubmitTemplateBased() {
    console.log(JSON.stringify(this.model));
  }

  constructor() { }

  ngOnInit() {
  }

}
