import { FormControl, ValidatorFn } from "@angular/forms";
import { isNumber } from "util";

export const phoneNumberValidator: ValidatorFn  = (control: FormControl)  =>{
    return isNumber(control.value) ? {
      phone: true
    } : null;
}