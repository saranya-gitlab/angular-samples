import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
    myForm: FormGroup;

    constructor(private fb: FormBuilder) {}

    ngOnInit() { 
        this.myForm = this.fb.group({
            name: ['Sammy', Validators.required],
            email: ['a', [Validators.required, Validators.email]],
            phone: ['123456789', [Validators.required, this.validateMobileNo]],
            message: ['a', [Validators.required, Validators.minLength(15)]],
            passwords:this.fb.group({
              pass1: ['', Validators.required],
              pass2: ['', Validators.required],
            },{
              validator:this.passwordMatch
            })
          });
          this.myForm.get('phone').valueChanges.subscribe(value => {
            this.disableEmail();
          });
    }
    passwordMatch= (control: AbstractControl) => {
      if(control.get("pass1").value !== control.get("pass2").value){
        return {notTheSame:true};
      }else{
        return null;
      }
    }
    
    validateMobileNo(controller: AbstractControl): { [key: string]: any } {        
      console.log(controller.value);
    
      if (isNaN(controller.value) || new String(controller.value).length >10) {
        console.log("Inside If Block");
        return {
          mobileNoInvalide: true
        };
      }
      console.log("Returning null");
      return null;
    }



    disableEmail(){
      if(this.myForm.get('phone').invalid){
        this.myForm.get('email').enable();
      }else{
        this.myForm.get('email').disable();
      }
     
    }
    onSubmit(form: FormGroup) {
      console.log('Valid?', form.valid); // true or false
      console.log('Name', form.value.name);
      console.log('Email', form.value.email);
      console.log('Message', form.value.message);
    }
}
