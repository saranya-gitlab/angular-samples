import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';

let data = {"page":1,"per_page":6,"total":12,"total_pages":2,
"data":[{"id":1,"email":"george.bluth@reqres.in","first_name":"George","last_name":"Bluth","avatar":"https://reqres.in/img/faces/1-image.jpg"},{"id":2,"email":"janet.weaver@reqres.in","first_name":"Janet","last_name":"Weaver","avatar":"https://reqres.in/img/faces/2-image.jpg"},{"id":3,"email":"emma.wong@reqres.in","first_name":"Emma","last_name":"Wong","avatar":"https://reqres.in/img/faces/3-image.jpg"},{"id":4,"email":"eve.holt@reqres.in","first_name":"Eve","last_name":"Holt","avatar":"https://reqres.in/img/faces/4-image.jpg"},{"id":5,"email":"charles.morris@reqres.in","first_name":"Charles","last_name":"Morris","avatar":"https://reqres.in/img/faces/5-image.jpg"},{"id":6,"email":"tracey.ramos@reqres.in","first_name":"Tracey","last_name":"Ramos","avatar":"https://reqres.in/img/faces/6-image.jpg"}],"support":{"url":"https://reqres.in/#support-heading","text":"To keep ReqRes free, contributions towards server costs are appreciated!"}};
describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpClient, UserService],
      imports: [ HttpClientModule ],
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('should return null', inject([UserService], (service: UserService) => {
    expect(service.getUser()).toBeNull();
  }));

  it('get should not return null', inject([UserService], (service: UserService) => {
    let users = [];
    service.get('users?page=1').subscribe(res => {
      users = res.data;
      // expect(res).toEqual(data);
      expect(users.length).toEqual(6);
    });
  }));

  it('get should not return null', inject([UserService], (service: UserService) => {
    let users = [];
    service.get('users?page=1').subscribe(res => {
      users = res.data;
      // expect(res).toEqual(data);
      expect(users.length).toEqual(6);
    });
  }));

  it('post should not return null', inject([UserService], (service: UserService) => {
    let newUser = {
      "name": "Angular 2",
      "job": "developer"
    } ;
    service.post('users', newUser).subscribe(res => {
      let val = res.id;
      // expect(res).toEqual(data);
      expect(res.id).toBeGreaterThan(1);
    });
  }));
});
