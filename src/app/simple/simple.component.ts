import { Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.css']
})
export class SimpleComponent implements OnInit, OnChanges, DoCheck {
@Input() simpleInput: string;

  constructor() {
    console.log("I am a constructor");
   }

  ngOnInit() {
    console.log('oN INIT');
  }

  ngOnChanges(changes : SimpleChanges){
    for (let propertyName in changes){
      let change = changes[propertyName];
      let current = JSON.stringify(change.currentValue);
      let previous = JSON.stringify(change.previousValue);
      console.log(propertyName + ': currentValue = ' +current + ', previousValue =' + previous);
    }
  }

  ngDoCheck(){
    console.log('I am a do check');
  }
}
