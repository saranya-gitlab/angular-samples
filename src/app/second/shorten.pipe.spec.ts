import { ShortenPipe } from './shorten.pipe';

describe('shorten', () => {
  let pipe = new ShortenPipe();

  it('return same value if undefined', () => {
    expect(pipe.transform("Hello", undefined)).toBe("Hello");
  });

  it('transform minutes to hours and minutes string', () => {
    const testData = {
        "Hello": 'Hel...',
        "Saranya": 'Sar...'
    };

    Object.keys(testData).forEach((key) => {
        expect(pipe.transform(key, 3)).toBe(testData[key]);
    });
  });
});