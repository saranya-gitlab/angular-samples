import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { interval } from 'rxjs';
import { of, pipe } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {
  value = 'Hello All';
  date = new Date();
  obj = { id: 1, value: 'asdasd' };
  users: any;
  messageSuccess;
  constructor(private api: UserService) { }

  ngOnInit() {
    console.log("Before API");
    this.api.get('users?page=1').subscribe({
      next(response) { this.users = response.data; console.log(response); },
      error(err) { console.error('Error: ' + err); },
      complete() { console.log('Completed'); }
    });

    // const secondsCounter = interval(5000);
    // const subscription = secondsCounter.subscribe(n =>
    // console.log(`It's been ${n + 1} seconds since subscribing!`));

    const nums = of(1, 2, 3, 4, 5);

    const squareOdd = this.squareOddVals(nums);
    console.log(squareOdd);
    squareOdd.subscribe(x => console.log(x));


    const el = document.getElementById('my-element');

// Create an Observable that will publish mouse movements
const mouseMoves = fromEvent(el, 'mousemove');

// Subscribe to start listening for mouse-move events
const subscription1 = mouseMoves.subscribe((evt: MouseEvent) => {
  // Log coords of mouse movements
  console.log(`Coords: ${evt.clientX} X ${evt.clientY}`);

  // When the mouse is over the upper-left of the screen,
  // unsubscribe to stop listening for mouse movements
  if (evt.clientX < 40 && evt.clientY < 40) {
    subscription1.unsubscribe();
  }
});
    
  }
  squareOddVals = pipe(
    filter((n: number) => n % 2 !== 0),
    map(n => n * n)
  );
}
