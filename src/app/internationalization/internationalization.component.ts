import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-internationalization',
  templateUrl: './internationalization.component.html',
  styleUrls: ['./internationalization.component.css']
})
export class InternationalizationComponent implements OnInit{
  name = 'Translation';

  constructor(public translate: TranslateService) {

  }

  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'vi']);
    this.translate.setDefaultLang('en');

    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|fr|vi/) ? browserLang : 'en');

    console.log(browserLang);
  }

}
