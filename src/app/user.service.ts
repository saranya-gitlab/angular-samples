import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { from } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { of } from 'rxjs';
const API_URL = 'https://reqres.in';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  public getUser(){
    return null;
  }
  public get(url): Observable<any>{
    return ajax(API_URL + '/api/' + url).pipe(
      map((res: any) => {
        if (!res.response) {
          console.log('Error occurred.');
          throw new Error('Value expected!');
        }
        return res.response;
      }),
      retry(3), // Retry up to 3 times before failing
      catchError(err => of([]))
    );
    // return from(fetch(API_URL + '/api/' + url));
  }

  public post(url, options): Observable<any> {
    return this.http.post(API_URL + '/api/' + url, options);
  }

  public put(url, options) {
    return this.http.put(API_URL + '/api/' + url, options);
  }

  public delete(url, options) {
    return this.http.delete(API_URL + '/api/' + url, options);
  }
}
