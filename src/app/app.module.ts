import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SimpleComponent } from './simple/simple.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { AlertComponent } from './alert/alert.component';
import { TableComponent } from './table/table.component';
import { FormComponent } from './form/form.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { FirstDirective } from './first.directive';
import { RedColorDirective } from './first/red-color.directive';
import { UserService } from './user.service';
import {ShortenPipe} from '././second/shorten.pipe';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { InternationalizationComponent } from './internationalization/internationalization.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    SimpleComponent,
    FirstComponent,
    SecondComponent,
    AlertComponent,
    TableComponent,
    FormComponent,
    ButtonsComponent,
    FirstDirective,
    RedColorDirective,
    ShortenPipe,
    InternationalizationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [HttpClient, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
